# Tango Request For Comments (RFC)

This repository is the home of all Tango Open Specification.

The process to add or change an RFC is the following:

- An RFC is created and modified by pull requests according to the Collective Code Construction Contract [(C4)](https://github.com/unprotocols/rfc/blob/master/1/README.md).
- The RFC life-cycle SHOULD follow the life-cycle defined in the Consensus-Oriented Specification System [(COSS)](https://github.com/unprotocols/rfc/blob/master/2/README.md).

## Current RFCs

Short Name   | Title                                                         | Type     | Status     | Editor
-------------|---------------------------------------------------------------|----------|------------|-------
[RFC-1](1)   | The Tango control system                                      | Standard | Draft      | Vincent Hardion
[RFC-2](2)   | The device object model                                       | Standard | Draft      | Vincent Hardion
[RFC-3](x)   | The command model                                             | Standard | Draft      | 
[RFC-4](x)   | The attribute model                                           | Standard | Draft      | 
[RFC-5](x)   | The property model                                            | Standard | Draft      | Vincent Hardion
[RFC-x](x)   | The database system                                           | Standard | Draft      | 
[RFC-x](x)   | The database system - Directory                               | Standard | Draft      | 
[RFC-x](x)   | The database system - Persistance                             | Standard | Draft      | 
[RFC-x](x)   | The server model                                              | Standard | Draft      | 
[RFC-x](x)   | The class model                                               | Standard | Draft      | 
[RFC-x](x)   | The dynamic attribute and command                             | Standard | Draft      | 
[RFC-x](x)   |                                                               | Standard | Draft      | 
[RFC-x](x)   | Logging service                                               | Standard | Draft      | 
[RFC-x](x)   | Cache system                                                  | Standard | Draft      | 
[RFC-x](x)   | Memorised attribute service                                   | Standard | Draft      | 
[RFC-x](x)   | Authorisation system                                          | Standard | Draft      | 
[RFC-x](x)   |                                                               | Standard | Draft      | 
[RFC-x](x)   | The Request-Reply protocol                                    | Standard | Draft      | 
[RFC-x](x)   | The Request-Reply protocol - CORBA implementation             | Standard | Draft      | 
[RFC-x](x)   | The Publisher-Subsciber protocol                              | Standard | Draft      | 
[RFC-x](x)   | The Publisher-Subsciber protocol - ZeroMQ implementation      | Standard | Draft      | 
[RFC-x](x)   |                                                               | Standard | Draft      | 
[RFC-x](x)   | High Level API                                                | Standard | Draft      | 
[RFC-x](x)   | High Level API - Python implementation                        | Standard | Draft      | 
[RFC-x](x)   | High Level API - Java   implementation                        | Standard | Draft      | 
[RFC-x](x)   |                                                               | Standard | Draft      | 
[RFC-x](x)   |                                                               | Standard | Draft      | 
